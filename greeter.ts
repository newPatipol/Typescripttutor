let say: string = "hello world";
//type array
let numlist: Array<number> = [1, 2];
let num2type = [3, "3"];
enum grade {
  A,
  B,
  C,
  D,
  F,
  I,
  U,
  W
}
let student1 = grade.A;
//type any
let val: any = 1;
val = "2";
val = [1, "3", true];

//type void
let val1: void = null;
let val2: void = undefined;
//Change type any to string
let someValue: any = "123";
let lengthOfValue: number = (<string>someValue).length;

// review how to define function in javascript
function nameOffunction() {
  return "some thing";
}
let func = function() {
  return "some things";
};
let arrowfunc = () => {
  return "some things";
};

//function in typescript return number
function foo(sample: number): number {
  return sample;
}
//function in typescript optional parameter
function human(fname: string, lname: string, age?: number): void {}
human("Nades", "arigoto");
human("Nades", "arigoto", 25);
//human("Nades") error

//function in typescript Default parameter
function addNum(num1: number = 0, num2: number = 0) {
  return num1 + num2;
}
addNum(1, 2);

//function overload in typescripts
function padding(all: number);
function padding(topAndBottom: number, leftAndRight: number);
function padding(top: number, right: number, bottom: number, left: number);

function padding(a: number, b?: number, c?: number, d?: number) {
  if (b === undefined && c === undefined && d === undefined) {
    b = c = d = a;
  } else if (c === undefined && d === undefined) {
    c = a;
    d = b;
  }
  return {
    top: a,
    right: b,
    bottom: c,
    left: d
  };
}
padding(1); // ok
padding(1, 1); // ok
// padding(1,1,1); error
padding(1, 1, 1, 1); // ok

//interface in typescript
interface Card {
  name: string;
  num: number;
}
let desk = (card: Card) => {
  return console.log(card.name + " " + card.num);
};
let cardpikaju: Card = {
  name: "pikaja",
  num: 3
};

desk(cardpikaju);
console.log(addNum(1));
console.log(say);
console.log(student1);

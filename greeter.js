var say = "hello world";
//type array
var numlist = [1, 2];
var num2type = [3, "3"];
var grade;
(function (grade) {
    grade[grade["A"] = 0] = "A";
    grade[grade["B"] = 1] = "B";
    grade[grade["C"] = 2] = "C";
    grade[grade["D"] = 3] = "D";
    grade[grade["F"] = 4] = "F";
    grade[grade["I"] = 5] = "I";
    grade[grade["U"] = 6] = "U";
    grade[grade["W"] = 7] = "W";
})(grade || (grade = {}));
var student1 = grade.A;
//type any
var val = 1;
val = "2";
val = [1, "3", true];
//type void
var val1 = null;
var val2 = undefined;
//Change type any to string
var someValue = "123";
var lengthOfValue = someValue.length;
// review how to define function in javascript
function nameOffunction() {
    return "some thing";
}
var func = function () {
    return "some things";
};
var arrowfunc = function () {
    return "some things";
};
//function in typescript return number
function foo(sample) {
    return sample;
}
//function in typescript optional parameter
function human(fname, lname, age) { }
human("Nades", "arigoto");
human("Nades", "arigoto", 25);
//human("Nades") error
//function in typescript Default parameter
function addNum(num1, num2) {
    if (num1 === void 0) { num1 = 0; }
    if (num2 === void 0) { num2 = 0; }
    return num1 + num2;
}
addNum(1, 2);
function padding(a, b, c, d) {
    if (b === undefined && c === undefined && d === undefined) {
        b = c = d = a;
    }
    else if (c === undefined && d === undefined) {
        c = a;
        d = b;
    }
    return {
        top: a,
        right: b,
        bottom: c,
        left: d
    };
}
padding(1); // ok
padding(1, 1); // ok
// padding(1,1,1); error
padding(1, 1, 1, 1); // ok
var desk = function (card) {
    return console.log(card.name + " " + card.num);
};
var cardpikaju = {
    name: "pikaja",
    num: 3
};
desk(cardpikaju);
console.log(addNum(1));
console.log(say);
console.log(student1);
